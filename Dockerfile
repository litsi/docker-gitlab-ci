FROM debian:bookworm

ENV DEBIAN_FRONTED noninteractive

RUN apt-get -qq update
RUN apt-get install -qqy \
      apt-transport-https \
      curl \
      git \
      gnupg \
      openjdk-17-jdk \
      unzip \
      wget

# Nodejs and Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    curl -sL https://deb.nodesource.com/setup_18.x > setup_18.x && \
    chmod +x setup_18.x && \
    ./setup_18.x && \
    apt-get update && \
    apt-get install -qqy nodejs yarn

# Android SDK
ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64/
ENV ANDROID_SDK_ROOT=/android-sdk-linux

RUN export JAVA_HOME && \
    wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-9477386_latest.zip && \
    unzip -qq -d android-sdk-linux android-sdk.zip && \
    mkdir android-sdk-linux/cmdline-tools/latest && \
    mv android-sdk-linux/cmdline-tools/NOTICE.txt android-sdk-linux/cmdline-tools/bin android-sdk-linux/cmdline-tools/lib android-sdk-linux/cmdline-tools/source.properties android-sdk-linux/cmdline-tools/latest/ && \
    android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "platforms;android-28" && \
    android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "build-tools;28.0.3" && \
    export ANDROID_HOME=$PWD/android-sdk-linux && \
    export PATH=$PATH:$PWD/android-sdk-linux/platform-tools/ && \
    yes | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager --licenses

